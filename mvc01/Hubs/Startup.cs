﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(mvc01.Hubs.Startup))]

namespace mvc01.Hubs
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}