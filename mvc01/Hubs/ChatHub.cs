﻿using Microsoft.AspNet.SignalR;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System;
using System.Diagnostics;


namespace mvc01.Hubs
{
    public class ChatHub : Hub
    {
        static ConcurrentDictionary<string, string> onlineUsers = new ConcurrentDictionary<string, string>();
        static Dictionary<int, List<string>> openChatRoomUsers = new Dictionary<int, List<string>>();      // Chat rooms and users
        static Dictionary<int, string> openChatRoomNames = new Dictionary<int, string>();
        static int openChatRoomId = 0;


        public void AllRoomUsers()
        {
            List<string> users = onlineUsers.Keys.ToList();
            Clients.Caller.parseUserList(users);

        }

        public void AllRoomUsers(int roomId)
        {
            Clients.Caller.parseChannelUserList(openChatRoomUsers[roomId]);
        }

        public void OpenRoom(string toUsername, string fromUser)
        {
            var toUser = onlineUsers.FirstOrDefault(u => u.Key == toUsername);

            if (openChatRoomId != 0)
            {
                for (int i = 1; i <= openChatRoomId; i++)
                {
                    if (openChatRoomUsers[i].Count == 2)
                    {
                        if ((openChatRoomUsers[i][0] == toUser.Key && openChatRoomUsers[i][1] == fromUser)
                            || (openChatRoomUsers[i][1] == toUser.Key && openChatRoomUsers[i][0] == fromUser))
                        {
                            Clients.Caller.privateMessageWindow(i, toUsername);
                            return;
                        }

                    }
                }
            }

            openChatRoomId++;

            openChatRoomUsers[openChatRoomId] = new List<string>();
            openChatRoomUsers[openChatRoomId].Add(toUser.Key);
            openChatRoomUsers[openChatRoomId].Add(fromUser);

            Clients.Caller.privateMessageWindow(openChatRoomId, toUser.Key);
        }

        public void UpdateChatRoom(int roomId, string username)
        {
            openChatRoomUsers[roomId].Add(username);
        }

        public void Send(string name, string message)
        {
            if (!String.IsNullOrWhiteSpace(message))
            {
                message = System.Net.WebUtility.HtmlEncode(message);
                message = message.Replace("\n", "<br />");
                var id = onlineUsers.FirstOrDefault(u => u.Key == name).Value;
                Clients.All.broadcastMessage(id, "[" + DateTime.Now.ToString("HH:mm:ss") + "]" + "    " + name + "     ", message);
            }
        }

        public void SendToSpecific(string name, string message, string to)
        {
            if (!String.IsNullOrWhiteSpace(message))
            {
                message = System.Net.WebUtility.HtmlEncode(message);
                message = message.Replace("\n", "<br />");
                Clients.Caller.yourPrivateMessage("[" + DateTime.Now.ToString("HH:mm:ss") + "]" + "    " + name + " -to- " + to + "     ", message);
                Clients.Client(onlineUsers[to]).privateMessage("[" + DateTime.Now.ToString("HH:mm:ss") + "]" + "    " + name + "     ", message);
            }
        }

        public void PrivateMessage(string chatId, string message)
        {
            string fromUserId = Context.ConnectionId.ToString();
            var fromUser = onlineUsers.FirstOrDefault(x => x.Value == fromUserId);

            string chatIdTrimmed = chatId.Trim('#', ' ');

            int chatRoomId = Convert.ToInt32(chatIdTrimmed);

            foreach (var item in openChatRoomUsers[chatRoomId])
            {
                if (item.Contains("[Disconnected]"))
                {
                    Clients.Caller.notifyOffline();
                    return;
                }
            }

            foreach (var item in openChatRoomUsers[chatRoomId])
            {
                var userId = onlineUsers.FirstOrDefault(x => x.Key == item).Value;

                Clients.Client(userId).sendPrivateMessage(chatIdTrimmed, fromUser.Key, message, item);
            }

            //string fromUserId = Context.ConnectionId.ToString();

            //if (String.IsNullOrEmpty(toId) || String.IsNullOrEmpty(message))
            //{
            //    return;
            //}

            //message = System.Net.WebUtility.HtmlEncode(message);
            //message = message.Replace("\n", "<br />");

            //var toUser = onlineUsers.FirstOrDefault(x => x.Key == toId);
            //var fromUser = onlineUsers.FirstOrDefault(x => x.Value == fromUserId);

            //if (!String.IsNullOrEmpty(toUser.Key) && !String.IsNullOrEmpty(fromUser.Key))
            //{
            //    Clients.Caller.sendPrivateMessage(toUser.Value, fromUser.Key, message, toUser.Key);
            //    Clients.Client(toUser.Value).sendPrivateMessage(fromUser.Value, fromUser.Key, message, fromUser.Key);
            //}
        }

        public void ReconnectToChats(string name)
        {
            if (name == "")
            {
                return;
            }

            var id = onlineUsers.Single(u => u.Key == name).Value;
            for (int i = 1; i <= openChatRoomUsers.Count; i++)
            {
                for (int j = 0; j < openChatRoomUsers[i].ToList().Count; j++)
                    if (openChatRoomUsers[i][j] == (name + "[Disconnected]"))
                    {
                        openChatRoomUsers[i][j] = name;
                        int comparison = 1;

                        if (comparison == j)
                            Clients.Client(id).privateMessageWindow(i, openChatRoomUsers[i][j - 1]);
                        else
                            Clients.Client(id).privateMessageWindow(i, openChatRoomUsers[i][j + 1]);
                        break;
                    }

            }
        }

        public void StartPrivateChat(string name)
        {
            var toUser = onlineUsers.FirstOrDefault(x => x.Key == name);
            Clients.Caller.privateMessageWindow(toUser.Value, toUser.Key);
        }

        public Boolean IsUsernameTaken(string name)
        {
            if (onlineUsers.ContainsKey(name))
            {
                return false;
            }

            return true;
        }

        public void Notify(string name, string id)
        {
            if (name == "")
            {
                Clients.Caller.refresh();
            }
            else
            {
                onlineUsers.TryAdd(name, id);
                foreach (KeyValuePair<string, string> entry in onlineUsers)
                {
                    Clients.Caller.online(entry.Key);
                }
                Clients.Others.enters(name);
            }
            // Sends a list containing of all usernames
            AllRoomUsers();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var name = onlineUsers.FirstOrDefault(x => x.Value == Context.ConnectionId.ToString());
            string s;

            if (name.Key != null)
            {
                onlineUsers.TryRemove(name.Key, out s);
                Clients.All.disconnected(name.Key);
            }

            for (int i = 1; i <= openChatRoomUsers.Count; i++)
            {
                for (int j = openChatRoomUsers[i].ToList().Count - 1; j > -1; j--)
                {
                    if (name.Key == openChatRoomUsers[i][j])
                    {
                        openChatRoomUsers[i][j] = openChatRoomUsers[i][j] + "[Disconnected]";
                    }
                }
            }

            return null;
        }
    }
}