﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mvc01.Hubs;

namespace mvc01.Controllers
{
    public class ChatController : Controller
    {
        // GET: Chat
        public ActionResult Index()
        {
            var name = TempData["username"];
            return View(name);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection form)
        {

            var username = form["username"];
            TempData["username"] = username;

            var chat = new ChatHub();

            if (String.IsNullOrEmpty(username))
            {
                ViewBag.Message = "Please insert a valid username";
                return View(form);
            }

            if (chat.IsUsernameTaken(username))
            {
                return RedirectToAction("Index");
            }

            ViewBag.Message = "Username is already taken";
            return View(form);
        }
    }
}