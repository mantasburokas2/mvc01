﻿using System.Linq;
using System.Web.Mvc;
using mvc01.DAL;
using mvc01.Models;
using System.Web.Security;
using System.Web;
using System;

namespace mvc01.Controllers
{
    public class UsersController : Controller
    {
        private UsersDBContext db = new UsersDBContext();

        // GET: Users
        public ActionResult Info()
        {
            return View(db.Users.ToList());
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(User user)
        {
            if (ModelState.IsValid)
            {
                if (IsValid(user.Email, user.Password))
                {
                    using (var db = new UsersDBContext())
                    {
                        string username = db.Users.FirstOrDefault(u => u.Email == user.Email).Username;
                        FormsAuthentication.SetAuthCookie(username, false);
                    }

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Please check log in information");
                }
            }
            return View(user);
        }

        [HttpGet]
        public ActionResult Edit()
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult Edit(HttpPostedFileBase image)
        {
            if(image != null)
            {
                using(db)
                {
                    string username = System.Web.HttpContext.Current.User.Identity.Name;

                    User user = db.Users.Where(u => u.Username == username).Single();

                    user.ProfilePicture = new byte[image.ContentLength];
                    image.InputStream.Read(user.ProfilePicture, 0, image.ContentLength);

                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult GetProfileImage()
        {
            using (db)
            {
                string username = System.Web.HttpContext.Current.User.Identity.Name;

                User user = db.Users.Where(u => u.Username == username).Single();

                return File(user.ProfilePicture, "image/png");
            }
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            if (ModelState.IsValid)
            {
                var count = db.Users.Count(u => u.Email == user.Email);

                if (count != 0)
                {
                    ModelState.AddModelError("", "Email already registered");
                }
                else
                {
                    using (db)
                    {
                        var crypto = new SimpleCrypto.PBKDF2();

                        var users = db.Users.Create();

                        users.Email = user.Email;
                        users.Password = crypto.Compute(user.Password);
                        users.Username = user.Username;
                        users.PasswordSalt = crypto.Salt;


                        db.Users.Add(users);
                        db.SaveChanges();

                        return RedirectToAction("LogIn", "Users");
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "Please check registration fields");
            }
            return View(user);
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        private bool IsValid(string email, string password)
        {
            bool isValid = false;

            var crypto = new SimpleCrypto.PBKDF2();

            using (db)
            {
                var user = db.Users.FirstOrDefault(e => e.Email == email);

                if (user != null)
                {
                    if (user.Password == crypto.Compute(password, user.PasswordSalt))
                    {
                        isValid = true;
                    }
                }

            }

            return isValid;
        }
    }
}
