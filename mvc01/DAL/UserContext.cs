﻿using mvc01.Models;
using System.Data.Entity;

namespace mvc01.DAL
{
    public class UsersDBContext : DbContext
    {
        public UsersDBContext()  : base("UsersDBContext") 
        {}

        public DbSet<User> Users { get; set; }
    }
}