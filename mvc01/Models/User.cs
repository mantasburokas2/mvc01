﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mvc01.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [EmailAddress]
        [Required]
        [Display(Name = "Email address: ")]
        public string Email { get; set; }

        public string Username { get; set; }

        [Display(Name = "Password: ")] 
        [Required]
        [DataType(DataType.Password)]
        [StringLength(200, MinimumLength=6)]
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public byte[] ProfilePicture { get; set; }

        public User GetUser(int id)
        {
            if (ID == id)
                return this;

            return null;
        }
        
    }
}