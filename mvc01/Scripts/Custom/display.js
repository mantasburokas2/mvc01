﻿
$(document).ready(function () {
    $('.msg').css("width", $('.messages').width() - 20 - 49 - 10);

    var size = $('footer').height();                // height of chat-container

    // Chat fixed until footer
    $(window).scroll(function () {

        var wsize = $(this).height();
        var footer = $('footer').offset().top;      // Number of pixels from top of window
        var scroll = $(this).scrollTop();           // Number of pixels hidden
        if (0 > footer - wsize - scroll) {
            $('#chat-container').css('bottom', (scroll + wsize - footer) + 'px');
        }
        else {
            $('#chat-container').css('bottom', '0px');
        }
    });
});

// Resizes messages written by user
function resizeMessage() {
    $('.msg').css("width", ($('#messages').width() - 20 - 49 - 10));
}

// Resizes messages written by users on screen window
$(window).resize(function () {
    $('.msg').css("width", $('#messages').width() - 20 - 49 - 10);
});
