﻿
var chat = $.connection.chatHub; //chat object

var channelArraySize = 1;
var channelArray = [];
var currentChannel = "#global";
channelArray[0] = "#global";

var userList = [];
var channelUsers = [];

$(function () {
    $('#wrapper').hide();
    startChatHub();             // Start connection to chat hub

    $(window).load(function () {
        $('#wrapper').show();
    })
});

$(document).ready(function () {
    $('#users').hide();
});

function startChatHub() {

    // Handling click event on username (Working just fine)
    $(document).on("click", "#onlineusers", function (event) {
        if ($(event.target).closest('.border').length > 0) {
            var name = ($(event.target).closest('.border')).text();
            if (!name.startsWith('You: ')) {
                chat.server.openRoom(name, $('#nickname').val());
            }
        }
    });

    chat.client.privateMessageWindow = function (id, name) {
        id = id.toString();
        if ($('#' + id).length == 0) {
            startPrivateMessage(id, name);
        }
        else {
            alert("You're already chatting with that user!");
        }
    }

    // Get user list of all online users
    chat.client.parseUserList = function (namesList) {
        userList = namesList;
    }

    chat.client.parseChannelUserList = function (nameList) {
        channelUsers = nameList;
    }

    chat.client.sendPrivateMessage = function (id, name, message, toUser) {
        if ($('#' + id).length == 0) {
            startPrivateMessage(id, name);
        }
        $('#' + id).append('<li class="messages"><img class="icon" src="http://ktyreapple.typepad.com/.a/6a0133f1e8b013970b0133f1f3cbda970b-pi"/><div class="msg">' + name + ': ' + message + '</div></li>');

        // Scroll down in chat
        $('#messages').scrollTop($('#messages').prop("scrollHeight"));
        // ---- From "display.js"
        resizeMessage();
    }

    $('.channel.global').click(function () {

        $('#users').hide();

        $(currentChannel).children().hide();
        $(currentChannel).hide();

        currentChannel = '#global';

        $(currentChannel).show();
        $(currentChannel).children().show();

        // ---- From "display.js"
        resizeMessage();
    });

    function startPrivateMessage(id, name) {

        var pm = '<div class="channel ' + id + '">' + name + '</div>';
        var $pm = $(pm);
        $('#dynamic-content').append($pm);

        var div = '<ul id="' + id + '" class="msgcont"></ul>'
        channelArray[channelArraySize] = '#' + id;
        channelArraySize++;
        for (i = 0; i < channelArray.length - 1; i++) {
            $(channelArray[i]).hide();
        }
        currentChannel = '#' + id;
        $('#users').show();

        var $div = $(div);

        $('#messages').append($div);

        $('.channel.' + id).click(function () {

            $(currentChannel).children().hide();
            $(currentChannel).hide();

            currentChannel = '#' + id;

            $(currentChannel).show();
            $(currentChannel).children().show();

            $('#users').show();

            // ---- From "display.js"
            resizeMessage();
        });

        //$('#message').keyup(function (e) {
        //    if (e.keyCode == 13) {
        //        console.log("Val: " + $('#message').val());
        //        if (!e.shiftKey) {
        //            console.log("Val: " + $(this).val());
        //            chat.server.privateMessage(id, name, $('#msg-container').find('#message').val());
        //        }
        //    }
        //});

    }

    chat.client.broadcastMessage = function (id, name, message) {

        $('#global').append('<li class="messages ' + id + '"><img class="icon" src="http://ktyreapple.typepad.com/.a/6a0133f1e8b013970b0133f1f3cbda970b-pi"/><div class="msg">' + name + ': ' + message + '</div></li>');

        // Scroll down in chat
        $('#messages').scrollTop($('#messages').prop("scrollHeight"));
        // ---- From "display.js"
        resizeMessage();
    };

    // Private message shown for target
    chat.client.privateMessage = function (name, message) {
        $('#global').append('<li class="messages ' + id + '"><img class="icon" src="http://ktyreapple.typepad.com/.a/6a0133f1e8b013970b0133f1f3cbda970b-pi"/><div class="msg pm">' + name + ': ' + message + '</div></li>');

        // Scroll down in chat
        $('#messages').scrollTop($('#messages').prop("scrollHeight"));
        // ---- From "display.js"
        resizeMessage();
    }

    // Private message shown for sender
    chat.client.yourPrivateMessage = function (name, message) {
        $('#global').append('<li class="messages ' + id + '"><img class="icon" src="http://ktyreapple.typepad.com/.a/6a0133f1e8b013970b0133f1f3cbda970b-pi"/><div class="msg own">' + name + ': ' + message + '</div></li>');

        // Scroll down in chat
        $('#messages').scrollTop($('#messages').prop("scrollHeight"));
        // ---- From "display.js"
        resizeMessage();
    }

    chat.client.online = function (name) {
        if (name == $('#nickname').val()) {
            $('#onlineusers').append('<div class="border" style="color:green">You: ' + name + '</div>');
        }
        else {
            $('#onlineusers').append('<div class="border">' + name + '</div>');

        }
        $('#users').append('<option value="' + name + '">' + name + '</option>');
    };

    // client joins the chat
    chat.client.enters = function (name) {
        $('#global').append('<li class="messages"><div class="notification join"><i>' + name + ' joins the conversation</i></div></li>');
        $('#onlineusers').append('<div class="border">' + name + '</div>');

        // Adding new user to user selection
        $('#users').append('<option value="' + name + '">' + name + '</option>');
        
        // Scroll down in chat
        $('#messages').scrollTop($('#messages').prop("scrollHeight"));
    };

    $('#users').change(function () {
        console.log($('#users').val());
    });

    // client leaves the page
    chat.client.disconnected = function (name) {
        $('#global').append('<li class="messages"><div class="notification leave"><i>' + name + ' leaves the conversation</i></div></li>');
        $('#onlineusers div').remove(":contains('" + name + "')");

        // Removes entry from user selection
        $('#users option[value="' + name + '"]').remove();

        // Scroll down in chat
        $('#messages').scrollTop($('#messages').prop("scrollHeight"));
    }

    chat.client.refresh = function () {
        window.location.href = redirectoToLogin;
    }

    //If the user writes a private message to a
    //offline user, notify him that he is offline
    chat.client.notifyOffline = function () {
        alert("The user is offline!");
    }

    $.connection.hub.start().done(function () {

        chat.server.notify($('#nickname').val(), $.connection.hub.id);
        chat.server.reconnectToChats($('#nickname').val());

        $('#message').keyup(function (e) {
            if (e.keyCode == 13) {
                if (!e.shiftKey) {
                    if (currentChannel == '#global') {
                        chat.server.send($('#nickname').val(), $('#message').val());
                    }
                    else {
                        // This is where function for sending messages for specific channel should be
                        chat.server.privateMessage(currentChannel, $('#message').val());
                    }
                    // Set textarea as focused again
                    $('#message').val('').focus();
                }
            }
        });
    });

}