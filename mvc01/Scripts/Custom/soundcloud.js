﻿
var playing = false;        //whether sound is playing

SC.initialize({
    client_id: 'YOUR_CLIENT_ID'
});

$(document).ready(function () {
    SC.stream('/tracks/200754217', {
        ontimedcomments: function (comment) {
            $('#results').html(comment[0].body);
        },
        onfinish: function () {
            playig = false;
        },
        onstop: function () {
            playig = false;
        }
    },
        function (sound) {
            var playing = false;
            $('#playbtn').click(function () {
                if (playing == false) {
                    sound.play();
                    playing = true;
                }
                else {
                    sound.pause();
                    playing = false;
                }
            });
        });
});